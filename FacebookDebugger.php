<?php
	class FacebookDebugger {
		/*
		 * https://developers.facebook.com/docs/opengraph/using-objects
		 *
		 * Updating Objects
		 *
		 * When an action is published, or a Like button pointing to the object clicked,
		 * Facebook will 'scrape' the HTML page of the object and read the meta tags.
		 * The object scrape also occurs when:
		 *
		 *      - Every 7 days after the first scrape
		 *
		 *      - The object URL is input in the Object Debugger
		 *           http://developers.facebook.com/tools/debug
		 *
		 *      - When an app triggers a scrape using an API endpoint
		 *           This Graph API endpoint is simply a call to:
		 *
		 *           POST /?id={object-instance-id or object-url}&scrape=true
		 */
		public function reload ($url) {
			$scrape = false;
			$file = 'log.txt';

			while (!$scrape) {

				$graph = 'https://graph.facebook.com/';

				//! staging
				// $post = 'id=' . urlencode ($url) . '&scrape=true&access_token=858204864266067|rSm6TdHvRRWCUWjkgfuOPwjW8ww';

				//! live
				$post = 'id=' . urlencode ($url) . '&scrape=true&access_token=CAAMMiApGVz8BAHksah4c5ZCfeVsr8PZBZBStz9sCZBD3u8kZBX01Bsqxrfg57q3kQtKb4eBk4AigJQsEEAHS7FZCkdyuuoV8mihmNiRd21oIbtRyGFG37hzTS0e5neSvb0WB8DsZAGZCFkiy1sa9GyUkeuwWtzhIYY5B7Fa0ak6dYBYfHGx4IGlaxO1VqFVMUyOMVloBFF6nMKJydqPdOCkyKJZC7EahynosZD';

				$httpcode = $this -> send_post ($graph, $post);

				if ($httpcode != 504) {
					$scrape = true;
					sleep (3);

				} else {
					sleep (10);
				}
			}

			$scrape = ($scrape) ? 'true' : 'false';
			$data = $url . ' - ' . $scrape . "\r\n";

			file_put_contents ($file, $data, FILE_APPEND);
			echo $data;
		}

		private function send_post ($url, $post) {
			$r = curl_init ();
			curl_setopt ($r, CURLOPT_URL, $url);
			curl_setopt ($r, CURLOPT_POST, 1);
			curl_setopt ($r, CURLOPT_POSTFIELDS, $post);
			curl_setopt ($r, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($r, CURLOPT_CONNECTTIMEOUT, 5);
			$data = curl_exec ($r);
			$httpcode = curl_getinfo ($r, CURLINFO_HTTP_CODE);
			curl_close ($r);

			return $httpcode;
		}
	}
?>