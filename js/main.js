'use strict';

$(function() {

    //! images to preload
    var images = [];
    images.push('img/home/main.jpg');

    images.push('img/home/my/rosli.jpg');
    images.push('img/home/camera-1.jpg');
    images.push('img/home/ph/pepe.jpg');
    images.push('img/home/camera-2.jpg');
    images.push('img/home/camera-3.jpg');
    images.push('img/home/ph/mayad.jpg');
    images.push('img/home/camera-4.jpg');
    images.push('img/home/th/dusit.jpg');
    // images.push('img/home/more.jpg');

    images.push('img/home/my/rosli-mobile.jpg');
    images.push('img/home/ph/pepe-mobile.jpg');
    images.push('img/home/ph/mayad-mobile.jpg');
    images.push('img/home/th/dusit-mobile.jpg');
    // images.push('img/home/more-mobile.jpg');
    images.push('img/home/cameras-mobile.jpg');


    new preLoader(images, {
        onComplete: function(loaded, errors) {

            $('.preload').fadeOut(function() {
                $(this).remove();

                if (splash) {
                    // $('#content').css ('overflow-y', 'hidden');

                    TweenLite.delayedCall(1, function() {

                        reveal();

                        var slots = [];

                        for (var i = 0; i < 12; i++) {
                            slots[i] = i + 1;
                        }

                        // var switchInt = 14;
                        var switchInt = 5;
                        var randomInt = 0;

                        $('#splash').fadeIn(function() {

                            // setInterval(function() {
                            //     randomInt = getRandomInt(1, 12, switchInt);
                            //     var random = slots[randomInt - 1];

                            //     $('#splash .bg-' + random).fadeOut(function() {
                            //         $(this).removeClass('bg-' + random).addClass('bg-' + switchInt).fadeIn();

                            //         slots[randomInt - 1] = switchInt;
                            //         switchInt = random;
                            //     })

                            // }, 3000);

                        });

                        // function getRandomInt(min, max, prev) {
                        //     var result = Math.round(Math.random() * (max - min)) + min;

                        //     if (result === prev) {
                        //         return getRandomInt(min, max, prev);
                        //     }

                        //     return result;
                        // }
                    });

                } else {
                    reveal();
                }
            });
        }
    });

    msieversion();

    var splash;
    var country_url = getCountryFromUrl();

    if (country_url !== null) {
        country_url = country_url[0];
    }

    if ($('#splash').length > 0) {
        // splash = (typeof $.cookie('splash') === 'undefined') ? true : false;
        splash = true;

    } else if (country_url === 'sea' && country_url !== 'undefined') {
        splash = false;

        $('#content .videographer').css({
            'opacity': '0.8'
        });

    } else {
        splash = false;

        $('#content .videographer[data-country=' + country_url + ']').css({
            'opacity': '0.8'
        });
    }

    $('#content .videographer#videographer-more').css({
        'opacity': '0.8'
    });

    $("#country-select").change(function(e) {
        var country_url = $("select option:selected").attr('id').toUpperCase();

        if (typeof trackMs_link === 'function') {
            e.stopPropagation();

            trackMs_link('ms:ilc:landing:country-selector:' + country_url, 'ilc', 'ilc:landing:country-selector:' + country_url, 'microsite|ilc|landing|country-selector|' + country_url);
        }

        window.location.href = $("select option:selected").val();
    })

    function reveal() {

        //! load background image
        var windowWidth = $(window).width();

        function desktopBg() {
            $('#videographer-my-rosli .bg.desktop').backstretch('img/home/my/rosli.jpg');
            $('#videographer-camera-1 .bg.desktop').backstretch('img/home/camera-1.jpg');
            $('#videographer-ph-pepe .bg.desktop').backstretch('img/home/ph/pepe.jpg');
            $('#videographer-camera-2 .bg.desktop').backstretch('img/home/camera-2.jpg');
            $('#home-main .bg.desktop').backstretch('img/home/main.jpg');
            $('#videographer-camera-3 .bg.desktop').backstretch('img/home/camera-3.jpg');
            $('#videographer-ph-mayad .bg.desktop').backstretch('img/home/ph/mayad.jpg');
            $('#videographer-camera-4 .bg.desktop').backstretch('img/home/camera-4.jpg');
            // $('#videographer-coming-soon .bg.desktop').backstretch('img/home/more.jpg');
            $('#videographer-th-dusit .bg.desktop').backstretch('img/home/th/dusit.jpg');
        }

        function mobileBg() {
            $('#videographer-my-rosli .bg.mobile').backstretch('img/home/my/rosli-mobile.jpg');
            $('#videographer-ph-pepe .bg.mobile').backstretch('img/home/ph/pepe-mobile.jpg');
            $('#videographer-ph-mayad .bg.mobile').backstretch('img/home/ph/mayad-mobile.jpg');
            $('#videographer-th-dusit .bg.mobile').backstretch('img/home/th/dusit-mobile.jpg');
            // $('#videographer-coming-soon .bg.mobile').backstretch('img/home/more-mobile.jpg');
            $('#videographer-cameras .bg.mobile').backstretch('img/home/cameras-mobile.jpg');
        }


        mobileBg();
        desktopBg();

        // if (windowWidth <= 768) {
        //     mobileBg();

        // } else {
        //     desktopBg();

        //     if (getInternetExplorerVersion() === 8) {
        //         setTimeout(function() {
        //             desktopBg();
        //         }, 100);
        //     }
        // }

        // if (typeof country_url !== 'undefined') {

        //     if (country_url !== 'sg') {
        //         $('#home-main .bg').backstretch('img/home/main_2.jpg');

        //         if (getInternetExplorerVersion() === 8) {
        //             setTimeout(function() {
        //                 $('#home-main .bg').backstretch('img/home/main_2.jpg');
        //             }, 100);
        //         }

        //     } else if (country_url === 'sg') {
        //         $('#home-main .bg').backstretch('img/home/main.jpg');

        //         if (getInternetExplorerVersion() === 8) {
        //             setTimeout(function() {
        //                 $('#home-main .bg').backstretch('img/home/main.jpg');
        //             }, 100);
        //         }
        //     }
        // }

        $('header')
            .add($('#content'))
            .add($('footer'))
            .fadeIn();

        $('header .home').addClass('active');

        $(window).resize();

        $(window).load(function() {
            $("#country-select option:eq(0)").prop('selected', true);
        })
    }

    function msieversion() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            // If Internet Explorer, return version number
            $('#content .videographer').css({
                'border': '1px solid transparent',
            });

        } else {
            // If another browser, return 0
        }

        return false;
    }
});
