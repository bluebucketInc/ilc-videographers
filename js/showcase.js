'use strict';

$(function() {

    // different link for different country
    Handlebars.registerHelper('getLink', function(data, options) {
        var out = data.link[window.country];
        return out;
    });

    

    $.ajaxSetup({
        cache: true
    });

    //! load videos data
    $.ajax({
        url: 'data/' + language + '/vid-showcase.json',
        type: 'GET',
        dataType: 'JSON'
    })

    .done(function(data) { 
        
        var showcases = data;

        templatingContent();


        // magnificPopup
        var magnificPopupOptions = {
            type: 'inline',
            gallery: {
                enabled: true,
                preload: 0
            },
            closeOnBgClick: true,
            mainClass: 'mfp-fade',
            callbacks: {
                afterChange: function() {
                    this.content.find('img.lazy').lazyload();
                    // FB.XFBML.parse(this.content[0]);
                    $(window).resize();
                },
                open: function() {
                    this.content.find('img.lazy').lazyload();
                }
            }
        };

        $('#content .section-thumb').each(function() { // the containers for all your galleries
            $(this).find('.video-carousel-non-mobile').magnificPopup(magnificPopupOptions);
        });

        // $('#content .section-thumb .video-carousel-non-mobile').magnificPopup(magnificPopupOptions);
        // $('#content .video-carousel-mobile').magnificPopup(magnificPopupOptions);


        function templatingContent() {
            try {

                //! compile filter content
                var filterContentTemplate = Handlebars.compile($('#content #filter-content').html());
                var filterContentHTML = stripStupidSpacesFrontAndBack(filterContentTemplate(showcases));
                $('#content .filter-content').html(filterContentHTML);

                //! compile showcase content
                var showCaseContentTemplate = Handlebars.compile($('#content #showcase-content').html());
                var showCasesContentHTML = stripStupidSpacesFrontAndBack(showCaseContentTemplate(showcases));
                $('#content .showcase-content').html(showCasesContentHTML);

                //! compile showcase content
                var videoContentTemplate = Handlebars.compile($('#content #videos-content').html());
                var videoContentHTML = stripStupidSpacesFrontAndBack(videoContentTemplate(showcases));
                $('#content .videos-content').html(videoContentHTML);

                makeTextBoxesSameHeight();

            } catch (e) {
                console.log(e);
                $('#content #filter-content').remove();
                $('#content #showcase-content').remove();
            }

            $(window).lazyLoadXT();
        }

    });
    

    //-----------------------------------
    // Filter
    //-----------------------------------
    var modelsSelected = [];

    $('#filter-wrapper').on('click','.filter-boxes', function() {
        
        // prereq
        if (modelsSelected.length <= 0) {
            $('.section-container').addClass('hidden');
        }

        var model = $(this).data('model');
        
        if (!_.includes(modelsSelected, model)) {
            modelsSelected.push(model);
            $(this).addClass('active');
            $('#' + model).removeClass('hidden');
        } else {
            _.pull(modelsSelected, model);
            $(this).removeClass('active');
            $('#' + model).addClass('hidden');
        }

        // postreq
        if (modelsSelected.length <= 0) {
           $('.section-container').removeClass('hidden');
           $('.divider').removeClass('hidden');
        } else if (modelsSelected.length == 1) {
           $('.divider').addClass('hidden');
        } else if ((modelsSelected.length > 1)) { 
            $('.divider').removeClass('hidden');
            $(".section-container.hidden").next('.divider').addClass('hidden');
            // remove every last section's divider
            var s = $(".section-container:not(.hidden)").length

            $(".section-container:not(.hidden):eq("+ (s-1)+")").next('.divider').addClass('hidden');
        } 

    });

    $('#filter-wrapper').on('click','.filter-clear-wrapper', function() {
        modelsSelected = [];
        $("#filter-wrapper .filter-boxes").removeClass('active');
        $('.section-container').removeClass('hidden');
        $('.divider').removeClass('hidden');
    });

    $('header .showcase').addClass('active');

    //-----------------------------------
    // Filter CTA
    //-----------------------------------
    // mobileSecNavInit();

    // function mobileSecNavInit() {
    //     $('#filter-wrapper').delay(600).slideDown(400, function() {
    //         $('#filter-cta').addClass('filter-opened');
    //         enableMobileNav();
    //     });
    // }

    // function filterDropDown(){
    //     $('#filter-cta').addClass('filter-opened');
    //     $('#filter-wrapper').slideDown();
    //     $('.showcase-filters').addClass('opened');
    //     $("#filter-cta").children(".up, .down").toggle();
    // }

    // enableMobileNav();

    // !! function enableMobileNav() {
    $('#filter-cta').click(function() {
        if ($(this).hasClass('filter-opened')) {
            $('#filter-wrapper').slideUp();
            $('.showcase-filters').removeClass('opened');
            $(this).removeClass('filter-opened');
            $(this).children(".up, .down").toggle();
        } else {
            $('#filter-wrapper').slideDown();
            $('.showcase-filters').addClass('opened');
            $(this).addClass('filter-opened')
            $(this).children(".up, .down").toggle();
        }

    })
    // }


    //----------------------------------------
    // filter boxes/ text height :: Mobile
    //----------------------------------------

    // var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    var windowWidth = window.innerWidth;

    function makeHeightSameAsWidth(){
        $(".filter-boxes").each(function(){

            // !! Make mobile filter boxes height same as width
            var boxWidth = $(this).width();
            $(this).height(boxWidth);

            // !!  Equal height for all Camera wrapper
            var imgHeightArray = [];
            var totalImgs = $("#showcase #content .filter-boxes .i-left .camera").length;

            $("#showcase #content .filter-boxes .i-left .camera").each(function(){
                var img = $(this).children('img').height();
                imgHeightArray.push(img);

                var arrayLength = imgHeightArray.length;

                if (arrayLength == totalImgs) {
                    var maxValueInArray = Math.max.apply(Math, imgHeightArray);
                    $("#showcase #content .filter-boxes .i-left .camera").height(maxValueInArray);
                }
            })
        })
    }

    function makeTextBoxesSameHeight() {
 
        var maxheight = 0;
 
        $("div.thumb-wrapper p").css('height','auto');
 
        $("div.thumb-wrapper p").each(function() {
            if ($(this).height() > maxheight) {
                maxheight = $(this).height();
            }
        });
 
        $("div.thumb-wrapper p").height(maxheight + 2);
    }

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });

        if (windowWidth <= 991) {
            makeHeightSameAsWidth();
            $(".filters-wrapper").hide();

            $(window).resize(function(){
                makeHeightSameAsWidth();
                makeTextBoxesSameHeight();
            })
        }
        

    });



});
