'use strict';

$(function() {

    // different link for different country
    Handlebars.registerHelper('getLink', function(data, options) {
        var out = data.link[window.country];
        return out;
    });

    $.ajaxSetup({
        cache: true
    });

    //! load FB SDK
    $.getScript('//connect.facebook.net/en_UK/all.js', function() {

        FB.init({
            appId: config.fbId,
            xfbml: false,
            version: 'v2.3'
        });

    });

    //! load the videos data
    $.ajax({
        url: 'data/' + language + '/videos.json',
        type: 'GET',
        dataType: 'JSON'
    })

    .done(function(data) {

        //! get the videographer ID
        var videographerId = getUrlVar('p');
        var videographerProfile = getProfile();
        var videographerCountry = getVideographerCountry(videographerProfile);
        var videos;
        var videographer;
        // var videographerTracking;

        //! filter out the videographer's videos
        videos = _.filter(data, function(n) {
            return n.videographer.nick == videographerProfile;
        });

        videos = _.sortByOrder(videos, ['id'], ['desc']);

        videographer = _.chain(videos).take(1).pluck('videographer').pluck('nick').first().value();

        // videographerTracking = _.chain(videos).take(1).pluck('videographer').pluck('tracking').value();

        templatingContent();

        // try {

        //     //! compile video content
        //     var videosContentTemplate = Handlebars.compile($('#content #videos-content').html());
        //     var videosContentHTML = stripStupidSpacesFrontAndBack(videosContentTemplate(videos));
        //     $('#content .videos-content').html(videosContentHTML);

        //     //! compile video carousel - desktop
        //     var videosCarouselTemplate = Handlebars.compile($('#content #videos-carousel').html());
        //     var videosCarouselHTML = stripStupidSpacesFrontAndBack(videosCarouselTemplate(videos));
        //     $('#content .videos-carousel').html(videosCarouselHTML);

        //     //! compile video carousel - Mobile
        //     var videosCarouselTemplate = Handlebars.compile($('#content #videos-carousel-mobile').html());
        //     var videosCarouselHTML = stripStupidSpacesFrontAndBack(videosCarouselTemplate(videos));
        //     $('#content .videos-carousel-mobile').html(videosCarouselHTML);

        // } catch (e) {
        //     // console.log(e);

        //     $('#content #videos-content').remove();
        //     $('#content #videos-carousel').remove();
        //     $('#content #videos-carousel-mobile').remove();
        // }

        // $(window).lazyLoadXT();

        // $('img.lazy').lazyload({
        //     effect: 'fadeIn'
        // });

        //! load background image
        var windowWidth = $(window).width();

        //! mobile
        if (windowWidth <= (768 - 1)) {
            $('#content').css({
                'background-image': 'url(img/profile/' + videographerCountry + '/' + videographerProfile + '/bg-mobile.png)'
            });

            //! tablet
        } else if ((windowWidth >= 768) && (windowWidth <= (992 - 1))) {
            $('#content').css({
                'background-image': 'url(img/profile/' + videographerCountry + '/' + videographerProfile + '/bg-tablet.png)'
            });

            //! desktop
        } else {
            $('#content').css({
                'background-image': 'url(img/profile/' + videographerCountry + '/' + videographerProfile + '/bg.png)'
            });
        }

        $('.secondary-nav').css({
            'background-image': 'url(img/profile/' + videographerCountry + '/bg-secondary_nav.png)'
        });

        $('.secondary-nav').find('#' + videographerProfile).addClass('active');
        $('.secondary-nav .nav_countries').find('#' + videographerCountry).addClass('active');

        var country_url = getCountryFromUrl()[0];

        // if (country_url !== 'sg') {
        //     $('#content .video-content .logo a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
        //         'opacity': 1
        //     });
        //     $('#content .video-content .lens a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
        //         'opacity': 1
        //     });
        // }

        //! activate carousel
        $('#content .videos-carousel').slick({
            slidesToShow: 6,
            nextArrow: '<a class="slick-next"><img src="img/carousel-next.png"></a>',
            prevArrow: '<a class="slick-prev"><img src="img/carousel-prev.png"></a>',
            lazyLoad: 'ondemand'
        });

        var magnificPopupOptions = {
        // $('#content .video-carousel, #content .video-carousel-mobile').magnificPopup({
            type: 'inline',
            gallery: {
                enabled: true,
                preload: [0, 2]
            },
            closeOnBgClick: true,
            mainClass: 'mfp-fade',
            callbacks: {
                change: function() {
                    var popupContent = this.content;
                    setTimeout(function() {
                        $('img.lazy').lazyload() /*.trigger('')*/ ;
                        FB.XFBML.parse(document.getElementById(popupContent[0].id));
                    }, 100);

                },
                afterChange: function() {
                    this.content.find('img.lazy').lazyload();
                    FB.XFBML.parse(this.content[0]);
                    $(window).resize();
                    // $(window).lazyLoadXT();
                }
            }
        };

        $('#content .video-carousel').magnificPopup(magnificPopupOptions);
        $('#content .video-carousel-mobile').magnificPopup(magnificPopupOptions);

        $('img.lazy').lazyload({
            effect: 'fadeIn'
        });

        $("#kol-select").change(function(e) {
            var country_url = $("select#kol-select option:selected").attr('id').toUpperCase();

            if (typeof trackMs_link === 'function') {
                e.stopPropagation();

                trackMs_link('ms:ilc:videographer:landing:country-selector:' + country_url, 'ilc:videographer', 'ilc:videographer:landing:country-selector:' + country_url, 'microsite|ilc:videographer|landing|country-selector|' + country_url);
            }

            window.location.href = $("select#kol-select option:selected").val();
        });

        $('body').on('click', 'button.mfp-arrow.mfp-arrow-right.mfp-prevent-close', function() {

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:videographer:' + country + ':profile:' + videographerProfile + ':video:rightArrow', 'ilc:videographer:' + country, 'ilc:videographer:' + country + ':profile:' + videographerProfile + ':video:rightArrow', 'microsite|ilc:videographer:' + country + '|profile|' + videographerProfile + '|video|rightArrow');

                } else {
                    trackMs_link('ms:ilc:videographer:' + country + ':' + language + ':profile:' + videographerProfile + ':video:rightArrow', 'ilc:videographer:' + country + ':' + language, 'ilc:videographer:' + country + ':' + language + ':profile:' + videographerProfile + ':video:rightArrow', 'microsite|ilc:videographer:' + country + ':' + language + '|profile|' + videographerProfile + '|video|rightArrow');

                }
            }
        });

        $('body').on('click', 'button.mfp-arrow.mfp-arrow-left.mfp-prevent-close', function() {

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:videographer:' + country + ':profile:' + videographerProfile + ':video:leftArrow', 'ilc:videographer:' + country, 'ilc:videographer:' + country + ':profile:' + videographerProfile + ':video:leftArrow', 'microsite|ilc:videographer:' + country + '|profile|' + videographerProfile + '|video|leftArrow');

                } else {
                    trackMs_link('ms:ilc:videographer:' + country + ':' + language + ':profile:' + videographerProfile + ':video:leftArrow', 'ilc:videographer:' + country + ':' + language, 'ilc:videographer:' + country + ':' + language + ':profile:' + videographerProfile + ':video:leftArrow', 'microsite|ilc:videographer:' + country + ':' + language + '|profile|' + videographerProfile + '|video|leftArrow');

                }
            }
        });

        $('.back-to-top').on('click', function() {

            $('html, body').stop().animate({
                scrollTop: 0
            });
        })

        //! for scroll bar appear;
        $(window).trigger("resize");

        $(window).load(function() {
            $("#kol-select option:eq(0)").prop('selected', true);

            $('.preload').fadeOut(function() {
                $(this).remove();
            });
        });

        function templatingContent() {
            try {

                //! compile video content
                var videosContentTemplate = Handlebars.compile($('#content #videos-content').html());
                var videosContentHTML = stripStupidSpacesFrontAndBack(videosContentTemplate(videos));
                $('#content .videos-content').html(videosContentHTML);

                //! compile video carousel - desktop
                var videosCarouselTemplate = Handlebars.compile($('#content #videos-carousel').html());
                var videosCarouselHTML = stripStupidSpacesFrontAndBack(videosCarouselTemplate(videos));
                $('#content .videos-carousel').html(videosCarouselHTML);

                //! compile video carousel - Mobile
                var videosCarouselTemplate = Handlebars.compile($('#content #videos-carousel-mobile').html());
                var videosCarouselHTML = stripStupidSpacesFrontAndBack(videosCarouselTemplate(videos));
                $('#content .videos-carousel-mobile').html(videosCarouselHTML);

            } catch (e) {
                // console.log(e);

                $('#content #videos-content').remove();
                $('#content #videos-carousel').remove();
                $('#content #videos-carousel-mobile').remove();
            }


            $(window).lazyLoadXT();

        }
    })

    .fail(function() {
        // console.log("error");
    })

    .always(function() {
        // console.log("complete");
    });
});
