'use strict';

$(function() {

    $.ajaxSetup({
        cache: true
    });

    //! load FB SDK
    $.getScript('//connect.facebook.net/en_UK/all.js', function() {

        FB.init({
            appId: config.fbId,
            xfbml: true,
            version: 'v2.3'
        });

    });

    var _videographerProfile = getProfileFromVideo();
    var _videographerCountry = getVideographerCountry(_videographerProfile);

    $('.secondary-nav').css({
        'background-image': 'url(img/profile/' + _videographerCountry + '/bg-secondary_nav.png)'
    });

    $('.secondary-nav').find('#' + _videographerProfile).addClass('active');
    $('.secondary-nav .nav_countries').find('#' + _videographerCountry).addClass('active');

    $('.secondary-nav .nav_middle').addClass('show').siblings('.' + _videographerCountry).removeClass('hidden');

    var country_url = getCountryFromUrl()[0];

    // if (country_url !== 'sg') {
    //     $('#content .video .logo a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
    //         'opacity': 1
    //     });
    // }

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });
    });
});
