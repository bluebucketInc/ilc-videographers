'use strict';

$(function() {

    // different link for different country
    Handlebars.registerHelper('getLink', function(data, options) {
        var out = data.link[window.country];
        return out;
    });

    $.ajaxSetup({
        cache: true
    });

    //! load FB SDK
    $.getScript('//connect.facebook.net/en_US/all.js', function() {

        FB.init({
            appId: config.fbId,
            xfbml: false,
            version: 'v2.3'
        });

    });

    //! load videos data
    $.ajax({
        url: 'data/' + language + '/videos.json',
        type: 'GET',
        dataType: 'JSON'
    })

    .done(function(data) {
        var videos = data;

        //! issue a count id to all videos to reference to the main video
        _.forEach(videos, function(video) {
            video.count = _.indexOf(videos, video) + 1;
        });

        var country_url = getCountryFromUrl()[0];

        templatingContent();

        $('header .gallery').addClass('active');
        $('.secondary-nav, .secondary-nav-mobile').css({
            'display': 'none'
        });

        $('#content .filters .filter').first().addClass('active');

        //! shuffle the video thumbnails
        $('#content .videos-carousel .video-carousel').shuffle();

        var videographer = '';
        var prev_videographer = '';

        var magnificPopupOptions = {
            type: 'inline',
            gallery: {
                enabled: true,
                preload: 0
            },
            closeOnBgClick: true,
            mainClass: 'mfp-fade',
            callbacks: {
                change: function() {
                    if (videographer !== '') {
                        prev_videographer = videographer;
                    }

                    videographer = $(this.content).data('videographer');
                },
                afterChange: function() {
                    this.content.find('img.lazy').lazyload();
                    FB.XFBML.parse(this.content[0]);
                    $(window).resize();
                    // $(window).lazyLoadXT();
                },
                open: function() {
                    this.content.find('img.lazy').lazyload();
                }
            }
        };

        $('#content .video-carousel-non-mobile').magnificPopup(magnificPopupOptions);
        $('#content .video-carousel-mobile').magnificPopup(magnificPopupOptions);



        /* * * * * * * * * * * * * * * * * * * * * * * * *
         * Set up video thumbnail grid layout :: STARTS
         * * * * * * * * * * * * * * * * * * * * * * * * */
        var filter;
        var wall = new freewall('#videos-wall');
        var wallMobile = new freewall('#videos-wall');
        var clear = false;

        wall.reset({
            selector: '.video-carousel-non-mobile',
            animate: true,
            cellW: 210,
            cellH: 'auto',
            onResize: function() {
                wall.refresh();
            }
        });

        wallMobile.reset({
            selector: '.video-carousel-mobile',
            animate: true,
            cellW: 90,
            cellH: 90,
            onResize: function() {
                if ($(window).width() <= 767) {
                    wallMobile.refresh();
                }
            }
        });

        wall.fitWidth();
        wallMobile.fitWidth();
        /* * * * * * * * * * * * * * * * * * * * * * * * *
         * Set up video thumbnail grid layout :: ENDS
         * * * * * * * * * * * * * * * * * * * * * * * * */



        $('img.lazy').lazyload({
            effect: 'fadeIn'
        });

        //! slide the filters out for desktop
        $('.filters').on('click', 'a.toggle', function(e) {
            e.preventDefault();

            $(this).siblings('.filters-wrapper').stop().slideToggle();
            $(this).find('.up').stop().fadeToggle();
            $(this).find('.down').stop().fadeToggle();

            if ($(this).parent().hasClass('open')) {

                if (typeof trackMs_link === 'function') {

                    if (language === '') {
                        trackMs_link('ms:ilc:' + country + ':gallery:menu:filter:collapse', 'ilc:' + country, 'ilc:' + country + ':gallery:menu:filter:collapse', 'microsite|ilc:' + country + '|gallery|menu|filter|collapse');

                    } else {
                        trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:menu:filter:collapse', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:menu:filter:collapse', 'microsite|ilc:' + country + ':' + language + '|gallery|menu|filter|collapse');
                    }
                }

            } else {

                if (typeof trackMs_link === 'function') {

                    if (language === '') {
                        trackMs_link('ms:ilc:' + country + ':gallery:menu:filter:expand', 'ilc:' + country, 'ilc:' + country + ':gallery:menu:filter:expand', 'microsite|ilc:' + country + '|gallery|menu|filter|expand');

                    } else {
                        trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:menu:filter:expand', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:menu:filter:expand', 'microsite|ilc:' + country + ':' + language + '|gallery|menu|filter|expand');
                    }
                }
            }

            $(this).parent().stop().toggleClass('open');

            return false;
        });

        //! clear filters
        $('.filters').on('click', 'a.clear', function(e) {
            e.preventDefault();

            clear = true;

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':gallery:menu:filter:clearAll', 'ilc:' + country, 'ilc:' + country + ':gallery:menu:filter:clearAll', 'microsite|ilc:' + country + '|gallery|menu|filter|clearAll');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:menu:filter:clearAll', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:menu:filter:clearAll', 'microsite|ilc:' + country + ':' + language + '|gallery|menu|filter|clearAll');
                }
            }

            $(this).parent().find('input').prop('checked', false);
            $('.filters input').first().change();

            return false;
        });

        /* mobile filter :: STARTS*/
        $('#filter').on('change', function(e) {
            var filter = $(e.target).val();

            if (filter === '') {
                return;
            }

            $('.drop-down-divider').removeClass('hidden');
            $('#camera').add($('#category')).add($('#kol')).add($('#media')).val('').addClass('hidden');
            $('#' + filter).removeClass('hidden');
        });

        $('#camera').add($('#category')).add($('#kol')).add($('#media')).on('change', function(e) {
            var filter = $(e.target).val();

            $('.filters input').prop('checked', false);
            $('.filters').find('input:checkbox[value="' + filter + '"]').prop('checked', true).change();
        });
        /* mobile filter :: ENDS*/

        //! filtering magic
        $('.filters').on('change', 'input', function(e) {
            $('.filters').find('input:checkbox[value="' + $(e.target).val() + '"]').prop('checked', $(e.target).prop('checked'));

            var _filtered;

            var type = $(this).prop('name');
            var checked_val = $(this).val();
            var ticked = $(this).is(":checked");

            if (ticked) {

                _filtered = _.filter(data, function(n) {

                    var results;

                    if (type == 'videographer') {
                        results = n['videographer']['nick'] == checked_val;
                    }

                    if (type == 'category') {
                        results = _.includes(n['category'], checked_val);
                    }

                    if (type == 'camera') {
                        results = n['camera']['name'] == checked_val;
                    }

                    if (type == 'media') {
                        results = _.includes(n['media'], checked_val);
                    }

                    return results;
                });

                var filtered_videographers = _.unique(_.pluck(_.flatten(_.pluck(_filtered, 'videographer')), 'nick'));

                var filtered_categories = [];

                if (type != 'category') {
                    filtered_categories = _.unique(_.flatten(_.pluck(_filtered, 'category')));
                }

                var filtered_cameras = _.unique(_.pluck(_.flatten(_.pluck(_filtered, 'camera')), 'name'));

                var filtered_medias = [];

                if (type != 'media') {
                    filtered_medias = _.unique(_.flatten(_.pluck(_filtered, 'media')));
                }

                var filtered_all = _.union(filtered_videographers, filtered_categories, filtered_cameras, filtered_medias);

                $.each(filtered_all, function(i, val) {
                    $('#cell-' + val).prop('checked', true);
                });

            } else {

                if (type == 'category') {

                    if ($('input:checkbox[name="category"]:checked').length <= 0) {
                        videos = [];
                    }
                }

                if (type == 'media') {

                    if ($('input:checkbox[name="media"]:checked').length <= 0) {
                        videos = [];
                    }
                }

                _filtered = _.filter(videos, function(n) {

                    var results;

                    if (type == 'videographer') {
                        results = n['videographer']['nick'] == checked_val;
                    }

                    if (type == 'category') {
                        results = _.every(n['category'], function() {
                            return _.includes(n['category'], checked_val);
                        });
                    }

                    if (type == 'camera') {
                        results = n['camera']['name'] == checked_val;
                    }

                    if (type == 'media') {
                        results = _.every(n['media'], function() {
                            return _.includes(n['media'], checked_val);
                        });
                    }

                    return !results;
                });

                var filtered_videographers = _.unique(_.pluck(_.flatten(_.pluck(_filtered, 'videographer')), 'nick'));

                var filtered_categories = _.unique(_.flatten(_.pluck(_filtered, 'category')));

                var filtered_cameras = _.unique(_.pluck(_.flatten(_.pluck(_filtered, 'camera')), 'name'));

                var filtered_medias = _.unique(_.flatten(_.pluck(_filtered, 'media')));

                var filtered_all = _.union(filtered_videographers, filtered_categories, filtered_cameras, filtered_medias);

                $('.filters').find('input:checkbox:checked').each(function() {

                    var exists = _.includes(filtered_all, $(this).val());

                    if (!exists) {
                        $(this).prop('checked', false);
                    }

                });

            }

            var videographers = [];
            $('.filters').find('input:checkbox[name="videographer"]:checked').each(function() {
                videographers.push($(this).val());
            });

            var categories = [];
            $('.filters').find('input:checkbox[name="category"]:checked').each(function() {
                categories.push($(this).val());
            });

            var cameras = [];
            $('.filters').find('input:checkbox[name="camera"]:checked').each(function() {
                cameras.push($(this).val());
            });

            var medias = [];
            $('.filters').find('input:checkbox[name="media"]:checked').each(function() {
                medias.push($(this).val());
            });

            videos = _.filter(data, function(n) {

                var results = (_.some(videographers, function(videographer) {

                    return _.includes(n['videographer']['nick'], videographer);

                }) || _.isEmpty(videographers)) && (_.some(categories, function(category) {

                    return _.includes(n['category'], category);

                }) || _.isEmpty(categories)) && (_.some(cameras, function(camera) {
                    return n['camera']['name'] == camera;

                }) || _.isEmpty(cameras)) && (_.some(medias, function(media) {

                    return _.includes(n['media'], media);

                }) || _.isEmpty(medias));

                return results;
            });

            videos = _.sortByOrder(videos, ['videographer.nick', 'id'], ['asc', 'desc']);

            if ($.isEmptyObject(videos)) {
                $('#content .message').fadeIn();

            } else {
                $('#content .message').fadeOut();
            }

            //! issue a count id to all videos to reference to the main video
            _.forEach(videos, function(video) {
                video.count = _.indexOf(videos, video) + 1;
            });

            // console.log(videos);

            templatingContent();

            //! shuffle the video thumbnails
            if (clear) {
                $('#content .videos-carousel .video-carousel').shuffle();
                clear = !clear;
            }

            $('#content .video-carousel').off('click.magnificPopup').removeData('magnificPopup');

            $('#content .video-carousel-non-mobile').magnificPopup(magnificPopupOptions);
            $('#content .video-carousel-mobile').magnificPopup(magnificPopupOptions);

            $('img.lazy').lazyload({
                effect: 'fadeIn'
            });

            $(window).trigger("resize");
        });

        $('#content .navbar-toggle').on('click', function(e) {
            e.preventDefault();

            if ($('#content .filters').hasClass('in') === false && $('#content .filters').hasClass('collapsing') === false) {
                $(this).find('.glyphicon').removeClass('glyphicon-menu-down');
                $(this).find('.glyphicon').addClass('glyphicon-menu-up');

            } else {
                $(this).find('.glyphicon').removeClass('glyphicon-menu-up');
                $(this).find('.glyphicon').addClass('glyphicon-menu-down');
            }

        });

        $('body').on('click', 'button.mfp-arrow.mfp-arrow-right.mfp-prevent-close', function() {

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':gallery:' + prev_videographer + ':video:rightArrow', 'ilc:' + country, 'ilc:' + country + ':gallery:' + prev_videographer + ':video:rightArrow', 'microsite|ilc:' + country + '|gallery|' + prev_videographer + '|video|rightArrow');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:' + prev_videographer + ':video:rightArrow', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:' + prev_videographer + ':video:rightArrow', 'microsite|ilc:' + country + ':' + language + '|gallery|' + prev_videographer + '|video|rightArrow');
                }
            }
        });

        $('body').on('click', 'button.mfp-arrow.mfp-arrow-left.mfp-prevent-close', function() {

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':gallery:' + prev_videographer + ':video:leftArrow', 'ilc:' + country, 'ilc:' + country + ':gallery:' + prev_videographer + ':video:leftArrow', 'microsite|ilc:' + country + '|gallery|' + prev_videographer + '|video|leftArrow');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:' + prev_videographer + ':video:leftArrow', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:' + prev_videographer + ':video:leftArrow', 'microsite|ilc:' + country + ':' + language + '|gallery|' + prev_videographer + '|video|leftArrow');
                }
            }
        });

        $('#content .videos-carousel').on({
            mouseenter: function() {
                var image = $(this).find('.border').siblings('img');

                $(this).find('.border').width(function() {
                    return image.width() - 14;
                });

                $(this).find('.border').height(function() {
                    return image.height() - 14;
                });

                $(this).find('.border').css({
                    'margin-top': '-' + image.height() + 'px'
                });
            },
            mouseleave: function() {}
        }, '.video-carousel');

        //! for scroll bar appear;
        $(window).trigger("resize");

        $(window).load(function() {

            $('.preload').fadeOut(function() {
                $(this).remove();
            });
        });

        function templatingContent() {
            try {

                //! compile video content
                var videosContentTemplate = Handlebars.compile($('#content #videos-content').html());
                var videosContentHTML = stripStupidSpacesFrontAndBack(videosContentTemplate(videos));
                $('#content .videos-content').html(videosContentHTML);

                //! compile video carousel
                var videosCarouselTemplate = Handlebars.compile($('#content #videos-carousel').html());
                var videosCarouselHTML = stripStupidSpacesFrontAndBack(videosCarouselTemplate(videos));
                $('#content .videos-carousel').html(videosCarouselHTML);

            } catch (e) {
                console.log(e);

                $('#content #videos-content').remove();
                $('#content #videos-carousel').remove();
            }

            // if (country_url !== 'sg') {
            //     $('#content .video-content .logo a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
            //         'opacity': 1
            //     });
            //     $('#content .video-content .lens a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
            //         'opacity': 1
            //     });
            // }

            $(window).lazyLoadXT();
        }
    })

    .fail(function() {
        // console.log("error");
    })

    .always(function() {
        // console.log("complete");
    });
});
