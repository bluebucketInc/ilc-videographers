'use strict';

var config = {
    // 'fbId': '858204864266067' //! staging
    'fbId': '858203357599551' //! live
};

//! to GET the var from the url
var getUrlVar = function(key) {
    var result = new RegExp(key + "=([^&]*)", "i").exec(window.location.search);
    return result && unescape(result[1]) || "";
};

var getProfile = function() {
    var regExp = /\w+?(?=\.html)/g;
    var profile = regExp.exec(window.location.pathname);
    return profile[0];
}

var getProfileFromVideo = function() {
    var regExp = /\w+?(?=\-\d.html)/g;
    var profile = regExp.exec(window.location.pathname);
    return profile[0];
}

var getCountryFromUrl = function() {
    var regExp = /(id|my|ph|sg|th|vn|sea|thai|viet)(?=\/(.+\.html)?)/g;
    return window.location.pathname.match(regExp);
}

var getVideographerCountry = function(videographer) {
    var country;

    switch (videographer) {

        // case "nicoline":
        //     country = 'id';
        //     break;


        case "rosli":
            country = 'my';
            break;


        case "pepe":
            country = 'ph';
            break;
        case "mayad":
            country = 'ph';
            break;


        // case "edward":
        //     country = 'sg';
        //     break;


        case "dusit":
            country = 'th';
            break;


        // case "nguyen":
        //     country = 'vn';
        //     break;

    }
    return country;
}

//! like the function name has said
var stripStupidSpacesFrontAndBack = function(str) {
    return str.replace(/^[\s\t\n\r\u200B]+|[\s\t\n\r\u200B]+$/g, '');
};

(function($) {

    //! to shuffle the order of dom elements
    $.fn.shuffle = function() {

        var allElems = this.get(),
            getRandom = function(max) {
                return Math.floor(Math.random() * max);
            },
            shuffled = $.map(allElems, function() {
                var random = getRandom(allElems.length),
                    randEl = $(allElems[random]).clone(true)[0];
                allElems.splice(random, 1);
                return randEl;
            });

        this.each(function(i) {
            $(this).replaceWith($(shuffled[i]));
        });

        return $(shuffled);
    };

    var country_url = getCountryFromUrl();

    if (country_url !== null && country_url.length === 2) {
        var language = country_url[1];

        $('.language select').val(language);

        $('.language select#language').css({
            'background-image': 'url(img/language-' + language + '.png)'
        });

        $('.language select#language-mobile').css({
            'background-image': 'url(img/language-' + language + '-mobile.png)'
        });
    }

    $('.language select').on('change', function(e) {
        var language = $(this).val();
        var url = window.location.pathname;

        if (country_url.length > 1) {
            url = url.replace(country_url[1] + '/', '');
        }

        if (language === 'en') {
            language = '';

        } else {
            language += '/';
        }

        url = url.replace(country_url[0] + '/', country_url[0] + '/' + language);
        window.location.href = url;
    });

})(jQuery);

var getInternetExplorerVersion = function() {
    //! Returns the version of Internet Explorer or a -1
    //! (indicating the use of another browser).

    var rv = -1; //! Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}


