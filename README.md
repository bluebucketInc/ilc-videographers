# Setup (for Mac)

## Installation of homebrew

	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

## Installation of node and npm

	brew install node

## Installation of bower

	npm install -g bower

## Installation of dependencies

Open the project directory at root level in terminal and run the following commands:

	bower install
	npm install

## Installation of gulp

	npm install -g gulp

## Updating of dependencies

	bower update
	npm update

***

# Development

## Compilation

Run `gulp build` to build/compile the files. The deployable folder is *dist*.

Run `gulp clean` to wipe the *dist* folder.

Run `gulp` to run `gulp clean` then `gulp build`.

### Compression and Minifying

Run `--compress` as a parameter to compress/minify the js and css files (i.e. `gulp --compress`).

### Staging

Run `--staging` as a parameter to compile the codes (i.e. `gulp --staging`). Use for staging.

### Production

Run `--production` as a parameter to compile the codes (i.e. `gulp --production`). Use for production.

There is 1 file that require editing (FB App ID) before compiling for production:

 * *js/helper.js*

#### *js/helper.js*

Comment away `'fbId': '771158533004598' //! staging` and enable `'fbId': '771157943004657' //! live`.

## Developing

Run `gulp server` to start local server. Whenever there are changes made to the following, the browser will reload on its own and re-build/re-compile the files:

* all *.js* files in the *js* folder and subfolders
* all *.less* files in the *less* folder and subfolders
* all *.jpg* and *.png* in the *img* folder and subfolders
* all *.template.html* files in the root and countries folders
* all *.html* files in the *template* folder

## Styling

All styling are to be done in the `less` folder in `.less` format.

# Plugins

## [Bootstrap](http://getbootstrap.com/)

Use for developing responsive web.

## [Handlebars.js](http://handlebarsjs.com/)

Template library to compile photos.

## [lodash](https://lodash.com/)

JS utility library.

## [slick](http://kenwheeler.github.io/slick/)

Use for carousel.

## [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/)

Use for lightbox.

## [Freewall](http://vnjs.net/www/project/freewall/)

Use for the grid layout.

## [Backstretch](http://srobbin.com/jquery-plugins/backstretch/)

Use to stretch image as background image. Mainly to tackle IE8 issue.

## [GreenSock GSAP](http://greensock.com/gsap)

Use for animation.

## [jQuery Cookie](https://github.com/carhartl/jquery-cookie)

Use for setting cookie.



# Login Credentials

## Staging

Server: ***dev.interuptive.com***

Username: ***www-sftp***

No password.

Remote path: ***sony.com.sg.dev.interuptive.com/public_html/alpha-kol/videographer***

Link: ***[http://sony-asia-new.dev.interuptive.com/alpha-kol/videographer](http://sony-asia-new.dev.interuptive.com/alpha-kol/videographer)***

## Live

Link: ***[http://www.sony-asia.com/microsite/ilc/videographer](http://www.sony-asia.com/microsite/ilc/videographer)***