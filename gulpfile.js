'use strict';

var gulp = require('gulp');
var del = require('del');
var browserSync = require('browser-sync');
var plugins = require('gulp-load-plugins')();
var merge = require('merge-stream');

var config = {
    'buildDir': 'dist/',
    'temp': '.tmp/',
    'development': !!plugins.util.env.development,
    'production': !!plugins.util.env.production,
    'staging': !!plugins.util.env.staging,
    'compress': !!plugins.util.env.compress,
    'domain': 'http://localhost:3000/',
    'fbId': '858204864266067'
};

if (config.production) {
    config.domain = 'http://www.sony-asia.com/microsite/ilc/videographers/';
    config.fbId = '858203357599551';

} else if (config.staging) {
    config.domain = 'https://tangosharklab.com/sonystaging/ilc/videographers/'; //http://sony-asia-new.dev.interuptive.com/ilc/videographers/
}

var onError = function(error) {
    plugins.util.beep();
    console.log(error);
};



/* ----- ----- ----- ----- ----- */
/* Generate individual video page */
/* ----- ----- ----- ----- ----- */
var data = require('./data/videos.json');
var generatingTasks = Object.keys(data);
var countries = ['sea', 'sg', 'id', 'my', 'ph', 'th', 'vn'];
var languages = ['thai', 'viet'];

generatingTasks.forEach(function(taskName) {

    gulp.task(taskName, ['__build'], function() {

        countries.forEach(function(country) {

            gulp.src(config.buildDir + country + '/video.html')
                .pipe(plugins.plumber({
                    errorHandler: onError
                }))
                .pipe(plugins.data(function(file) {
                    var data = require('./data/videos.json');
                    return data[taskName];
                }))
                .pipe(plugins.swig())
                .pipe(plugins.rename('video-' + data[taskName].videographer.nick + '-' + data[taskName].id + '.html'))
                .pipe(gulp.dest(config.buildDir + country + '/videos/'));

            languages.forEach(function(language) {

                gulp.src(config.buildDir + country + '/' + language + '/video.html')
                    .pipe(plugins.plumber({
                        errorHandler: onError
                    }))
                    .pipe(plugins.data(function(file) {
                        var data = require('./data/' + language + '/videos.json');
                        return data[taskName];
                    }))
                    .pipe(plugins.swig())
                    .pipe(plugins.rename('video-' + data[taskName].videographer.nick + '-' + data[taskName].id + '.html'))
                    .pipe(gulp.dest(config.buildDir + country + '/' + language + '/videos/'));
            });
        });
    });
});

gulp.task('videos', generatingTasks);



/* ----- ----- ----- ----- ----- */
/* Wipe whole "dist" folder */
/* ----- ----- ----- ----- ----- */
gulp.task('clean', ['clean:dist', 'clean:temp']);

gulp.task('clean:dist', function() {
    return del.sync([
        config.buildDir
    ]);
});

gulp.task('clean:temp', function() {
    return del.sync([
        config.temp
    ]);
});


/* ----- ----- ----- ----- ----- */
/* Optimise images */
/* ----- ----- ----- ----- ----- */
gulp.task('images', function() {
    return gulp.src(['img/**/*.jpg', 'img/**/*.png', 'img/**/*.gif', 'img/**/*.ico'])
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(gulp.dest(config.buildDir + 'img/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});



/* ----- ----- ----- ----- ----- */
/* SERVER */
/* ----- ----- ----- ----- ----- */
gulp.task('server', ['default'], function() {
    browserSync({
        server: {
            baseDir: config.buildDir,
        },
        reloadDelay: 3000,
        reloadDebounce: 3000,
        logLevel: 'debug'
    });

    gulp.watch(['**/*.template.html', 'template/**/*.html', 'js/**/*.js'], ['html-watch']);

    gulp.watch('less/**/*.less', ['less']);

    gulp.watch('data/**/*.json', ['json']);

    gulp.watch(['img/**/*.jpg', 'img/**/*.png', 'img/**/*.gif', 'img/**/*.ico'], ['images']);
});

gulp.task('html-watch', ['__build'], browserSync.reload);



/* ----- ----- ----- ----- ----- */
/* Less */
/* ----- ----- ----- ----- ----- */
gulp.task('less', function() {
    return gulp.src(['less/styles.less'])
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(plugins.less())
        .pipe(plugins.autoprefixer({
            browsers: [
                "Android 2.3",
                "Android >= 4",
                "Chrome >= 20",
                "Firefox >= 24",
                "Explorer >= 8",
                "iOS >= 6",
                "Opera >= 12",
                "Safari >= 6"
            ]
        }))
        .pipe(gulp.dest('css/'))
        .pipe(gulp.dest(config.buildDir + 'css/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});



/* ----- ----- ----- ----- ----- */
/* JSON */
/* ----- ----- ----- ----- ----- */
gulp.task('json', function() {
    return gulp.src('data/**/*.json')
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(config.compress ? plugins.jsonminify() : plugins.util.noop())
        .pipe(gulp.dest(config.buildDir + 'data/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});



/* ----- ----- ----- ----- ----- */
/* Templates */
/* ----- ----- ----- ----- ----- */
gulp.task('template', ['clean:temp'], function() {
    var files;

    if (config.development) {
        files = ['**/*.template.html', '!.tmp/**/*', '!id/**/*', '!my/**/*', '!ph/**/*', '!sea/**/*', '!th/**/*', '!vn/**/*'];

    } else {
        files = ['**/*.template.html', '!.tmp/**/*'];
    }

    return gulp.src(files)
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(plugins.debug())
        .pipe(plugins.fileInclude())
        .pipe(gulp.dest(config.temp));
});



/* ----- ----- ----- ----- ----- */
/* Bootstrap fonts */
/* ----- ----- ----- ----- ----- */
gulp.task('bootstrapFonts', function() {
    return gulp.src('bower_components/bootstrap/fonts/**/*')
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(plugins.changed(config.buildDir + 'fonts/'))
        .pipe(gulp.dest(config.buildDir + 'fonts/'));
});



/* ----- ----- ----- ----- ----- */
/* slick assets */
/* ----- ----- ----- ----- ----- */
gulp.task('slickAssets', function() {
    return merge(
        gulp.src(['bower_components/slick-carousel/slick/*.gif'])
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(plugins.changed(config.buildDir + 'css/'))
        .pipe(gulp.dest(config.buildDir + 'css/')),


        gulp.src('bower_components/slick-carousel/slick/fonts/*')
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(plugins.changed(config.buildDir + 'css/fonts/'))
        .pipe(gulp.dest(config.buildDir + 'css/fonts/'))
    );
});



/* ----- ----- ----- ----- ----- */
/* Main build function */
/* ----- ----- ----- ----- ----- */
gulp.task('__build', ['template'], function() {
    var assets = plugins.useref.assets();

    return gulp.src(config.temp + '**/*.template.html')
        .pipe(plugins.debug())
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(plugins.replace(/#{5} domain #{5}/g, config.domain))
        .pipe(plugins.replace(/#{5} fbId #{5}/g, config.fbId))
        .pipe(plugins.rename({
            extname: ''
        }))
        .pipe(plugins.rename({
            extname: '.html'
        }))
        .pipe(assets)
        .pipe(assets.restore())
        .pipe(plugins.useref())
        .pipe(gulp.dest(config.buildDir));
});

gulp.task('_build', ['__build', 'videos']);

gulp.task('build', ['bootstrapFonts', 'slickAssets', 'less', 'json', 'images', '_build'], function() {
    return merge(
        gulp.src(config.buildDir + 'js/**/*.js')
        .pipe(plugins.debug())
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(config.compress ? plugins.uglify({}) : plugins.util.noop())
        .pipe(gulp.dest(config.buildDir + 'js/')),


        gulp.src(config.buildDir + 'css/**/*.css')
        .pipe(plugins.debug())
        .pipe(plugins.plumber({
            errorHandler: onError
        }))
        .pipe(config.compress ? plugins.minifyCss({
            compatibility: 'ie8'
        }) : plugins.util.noop())
        .pipe(gulp.dest(config.buildDir + 'css/'))
    );
});



gulp.task('default', ['clean', 'build']);
